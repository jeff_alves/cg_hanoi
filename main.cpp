#include <GL/gl.h>
#include <GL/glut.h>

#include <cmath>
#include <iostream>
#include <list>

using namespace std;


const size_t NUMERO_DISCOS = 5;
const double ALTURA_PINOS = 3.0;

double FOV = 45.0;
size_t FPS = 60;
size_t prev_time = 0;
size_t window_width = 600, window_height = 600;

int animation = 1;
int directionalLight = 1;
int fullScreen = 1;

float lightAngle = 0.67, lightHeight = 10;
GLfloat angle = 0;   /* in degrees */
GLfloat angle2 = 30;   /* in degrees */

int moving, startx, starty;
int lightManualMoving = 0, lightStartX, lightStartY;
int lightAutoMove = 0;

GLfloat lightPosition[4];
GLfloat lightColor[] = {1.0, 1.0, 1.0, 1.0};

int selecionado = 3;

enum MENU_TYPE
{
    M_NONE,
    MENU_AJUDA,
    MENU_RESOLVER,
    MENU_INC_VEL,
    MENU_DEC_VEL,
    M_PAUSE,
    LIGHT_AUTO_MOTION,
    M_POSITIONAL,
    M_DIRECTIONAL,
    MENU_FULL_SCREEN,
    MENU_Exit
};

class CustomPoint {
public:
    double x;
    double y;
    double z;
    CustomPoint()
    {
        x = 0.0;
        y = 0.0;
        z = 0.0;
    }
    CustomPoint(double Set_X, double Set_Y, double Set_Z)
    {
        x = Set_X;
        y = Set_Y;
        z = Set_Z;
    }
    CustomPoint& operator= (CustomPoint const& obj)
    {
        x = obj.x;
        y = obj.y;
        z = obj.z;
        return *this;
    }
    CustomPoint operator-(CustomPoint const& p1)
    {
        return CustomPoint(x - p1.x, y - p1.y, z - p1.z);
    }
};

class Disco {
public:
    Disco()
    {
        normal = CustomPoint(0.0, 0.0, 1.0);
    }
    CustomPoint posicao;
    CustomPoint normal;   //orientacao
};

struct DiscoAtivo {    //Disco ativo a ser movido
    int index;
    CustomPoint pos_inicio, pos_fim;
    double u;		    // u E [0, 1]
    double passo_u;
    bool em_movimento;
    int direcao_rotacao;     // +1 Esquerda para Direita // -1 Direita para Esquerda, 0 = sem rotação
};

struct Pinos {
    CustomPoint posicoes[NUMERO_DISCOS];
    int ocupacao[NUMERO_DISCOS];
};

struct Tabuleiro {
    double x_min, y_min, x_max, y_max; //Base in XY-Plane
    double raio_base_pinos;               //raio da base dos Pinos
    Pinos pinos[3];
};

struct par_de_solucao {
    size_t de, para;
};

Disco discos[NUMERO_DISCOS];
Tabuleiro tabuleiro;
DiscoAtivo dico_ativo;
list<par_de_solucao> solucao;
bool resolver = false;

void initialize();
void initialize_game();
void display_handler();
void reshape_handler(int w, int h);
void keyboard_handler(unsigned char key, int x, int y);
void DrawPino(double x, double y, double r, double h);
void anim_handler();
void mouseWheel(int dir);
void visible(int vis);
void toggleFullScreen();
void mover_disco(int de, int para);
void hanoi_rec(int n, int de, int para);
void menu(int); // Menu handling function declaration
int main(int argc, char** argv);
CustomPoint get_inerpolated_coordinate(CustomPoint v1, CustomPoint v2, double u);

enum {
    X, Y, Z, W
};
enum {
    A, B, C, D
};

/* Create a matrix that will project the desired shadow. */
void shadowMatrix(GLfloat shadowMat[4][4], GLfloat groundplane[4], GLfloat lightpos[4])
{
    GLfloat dot;
    /* Find dot product between light posicao vector and ground plane normal. */
    dot = groundplane[X] * lightpos[X] +
          groundplane[Y] * lightpos[Y] +
          groundplane[Z] * lightpos[Z] +
          groundplane[W] * lightpos[W];

    shadowMat[0][0] = dot - lightpos[X] * groundplane[X];
    shadowMat[1][0] = 0.f - lightpos[X] * groundplane[Y];
    shadowMat[2][0] = 0.f - lightpos[X] * groundplane[Z];
    shadowMat[3][0] = 0.f - lightpos[X] * groundplane[W];

    shadowMat[X][1] = 0.f - lightpos[Y] * groundplane[X];
    shadowMat[1][1] = dot - lightpos[Y] * groundplane[Y];
    shadowMat[2][1] = 0.f - lightpos[Y] * groundplane[Z];
    shadowMat[3][1] = 0.f - lightpos[Y] * groundplane[W];

    shadowMat[X][2] = 0.f - lightpos[Z] * groundplane[X];
    shadowMat[1][2] = 0.f - lightpos[Z] * groundplane[Y];
    shadowMat[2][2] = dot - lightpos[Z] * groundplane[Z];
    shadowMat[3][2] = 0.f - lightpos[Z] * groundplane[W];

    shadowMat[X][3] = 0.f - lightpos[W] * groundplane[X];
    shadowMat[1][3] = 0.f - lightpos[W] * groundplane[Y];
    shadowMat[2][3] = 0.f - lightpos[W] * groundplane[Z];
    shadowMat[3][3] = dot - lightpos[W] * groundplane[W];

}

/* Setup floor plane for projected shadow calculations. */
/* Find the plane equation given 3 points. */
void findPlane(GLfloat plane[4], GLfloat v0[3], GLfloat v1[3], GLfloat v2[3])
{
    GLfloat vec0[3], vec1[3];

    /* Need 2 vectors to find cross product. */
    vec0[X] = v1[X] - v0[X];
    vec0[Y] = v1[Y] - v0[Y];
    vec0[Z] = v1[Z] - v0[Z];

    vec1[X] = v2[X] - v0[X];
    vec1[Y] = v2[Y] - v0[Y];
    vec1[Z] = v2[Z] - v0[Z];

    /* find cross product to get A, B, and C of plane equation */
    plane[A] = vec0[Y] * vec1[Z] - vec0[Z] * vec1[Y];
    plane[B] = -(vec0[X] * vec1[Z] - vec0[Z] * vec1[X]);
    plane[C] = vec0[X] * vec1[Y] - vec0[Y] * vec1[X];

    plane[D] = -(plane[A] * v0[X] + plane[B] * v0[Y] + plane[C] * v0[Z]);
}

GLfloat floorVertices[4][3] = {
        { -16.0, 0.0, 16.0 },
        { 16.0, 0.0, 16.0 },
        { 16.0, 0.0, -16.0 },
        { -16.0, 0.0, -16.0 },
};

/* Draw a floor (possibly textured). */
void drawFloor(void)
{
    glDisable(GL_LIGHTING);
    glBegin(GL_QUADS);
    glVertex3fv(floorVertices[0]);
    glVertex3fv(floorVertices[1]);
    glVertex3fv(floorVertices[2]);
    glVertex3fv(floorVertices[3]);
    glEnd();
    glEnable(GL_LIGHTING);
}

GLfloat floorPlane[4];
GLfloat floorShadow[4][4];

void mouse(int button, int state, int x, int y)
{
    switch(button) {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN) {
                moving = 1;
                startx = x;
                starty = y;
            }
            if (state == GLUT_UP) {
                moving = 0;
            }
            break;
        case GLUT_MIDDLE_BUTTON:
            if (state == GLUT_DOWN) {
                lightManualMoving = 1;
                lightStartX = x;
                lightStartY = y;
            }
            if (state == GLUT_UP) {
                lightManualMoving = 0;
            }
            break;
        case 3:  //mouse wheel scrolls
            mouseWheel(0);
            break;
        case 4:
            mouseWheel(1);
            break;
    }
}

void motion(int x, int y)
{
    if (moving) {
        angle = angle + (x - startx);
        angle2 = angle2 + (y - starty);
        startx = x;
        starty = y;
        glutPostRedisplay();
    }
    if (lightManualMoving) {
        lightAngle += (x - lightStartX)/80.0;
        lightHeight += (lightStartY - y)/40.0;
        lightStartX = x;
        lightStartY = y;
        glutPostRedisplay();
    }
}

void mouseWheel(int dir)
{
    if (dir > 0)
    {
        if (FOV > 99) FPS = 100;
        else FOV += 1;
        cout << "(+) FOV " << endl;
    }
    else
    {
        if (FOV <= 10) FPS = 10;
        else FOV -= 1;
        cout << "(-) FOV " << endl;
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(
            /* field of view in degree */ FOV,
            /* aspect ratio */   (GLfloat)window_width/(GLfloat)window_height,
            /* Z near */                  1.0,
            /* Z far */                  100.0
    );
    glMatrixMode(GL_MODELVIEW);
    glutPostRedisplay();
}

void special(int k, int x, int y)
{
    glutPostRedisplay();
}

void print_info()
{
    cout << "Torres de Hanoi" << endl;
    cout << "H:\t\tAjuda" << endl;
    cout << "ESC:\tSair" << endl;
    cout << "S:\t\tStart" << endl;
    cout << "+/-:\tControla velocidade" << endl;
    cout << "-----------------------------" << endl;
    cout << "Grupo:" << endl;
    cout << "\tJefferson Alves" << endl;
    cout << "\tLucas de Oliveira" << endl;
    cout << "-----------------------------" << endl;
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL | GLUT_MULTISAMPLE);
    glutInitWindowSize(window_width, window_height);
    glutCreateWindow("Torres de Hanoi");
    glutFullScreen();
    print_info();

    glutDisplayFunc(display_handler);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutVisibilityFunc(visible);
    glutReshapeFunc(reshape_handler);
    glutKeyboardFunc(keyboard_handler);
    glutIdleFunc(anim_handler);
    glutSpecialFunc(special);

    initialize_game();
    initialize();

    // Menu
    glutCreateMenu(menu);
    glutAddMenuEntry("Ajudar (H)", MENU_AJUDA);
    glutAddMenuEntry("Resolver (S)", MENU_RESOLVER);
    glutAddMenuEntry("Aumentar velocidade (+)", MENU_INC_VEL);
    glutAddMenuEntry("Reduzir velocidade (-)", MENU_DEC_VEL);
    glutAddMenuEntry("----------------------", M_NONE);
    glutAddMenuEntry("Pausar/Continuar", M_PAUSE);
    glutAddMenuEntry("Ligar/Desligar Movimencao da fonte de luz (C)", LIGHT_AUTO_MOTION);
    glutAddMenuEntry("----------------------", M_NONE);
    glutAddMenuEntry("Positional light (Z)", M_POSITIONAL);
    glutAddMenuEntry("Directional light (X)", M_DIRECTIONAL);
    glutAddMenuEntry("Ativar/Desativar tela cheia (F)", MENU_FULL_SCREEN);
    glutAddMenuEntry("-----------------------", M_NONE);
    glutAddMenuEntry("Sair (Q, Esc)", MENU_Exit);
    glutAttachMenu(GLUT_RIGHT_BUTTON);

    findPlane(floorPlane, floorVertices[1], floorVertices[2], floorVertices[3]);

    glutMainLoop();
    return 0;
}

void initialize()
{

    glPolygonOffset(-0.1, -0.1);

    glClearColor(0.1, 0.1, 0.1, 5.0); //Background Color
    glShadeModel(GL_SMOOTH);		  //SMOOTH Shading
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glLineWidth(3.0);


    glMatrixMode(GL_MODELVIEW);
    gluLookAt(0.0, 0.0, 30.0,  /* eye */
              0.0, 2.0, 0.0,      /* center */
              0.0, 1.0, 0.0);      /* up is in postivie Y direcao_rotacao */


    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);

    //Globals initializations
    prev_time = glutGet(GLUT_ELAPSED_TIME);
}

void initialize_game()
{
    //1) Tabuleiro
    tabuleiro.raio_base_pinos = 1.0;
    tabuleiro.x_min = 0.0;
    tabuleiro.x_max = 10 * tabuleiro.raio_base_pinos;
    tabuleiro.y_min = 0.0;
    tabuleiro.y_max = 3 * tabuleiro.raio_base_pinos;

    double x_center = 0;
    double y_center = 0;
    double dx = (tabuleiro.x_max - tabuleiro.x_min) / 3.0; // 3 Pinos

    // pinos
    for (size_t i = 0; i < 3; i++)
    {
        for (size_t h = 0; h < NUMERO_DISCOS; h++)
        {
            if (i == 0)
            {
                tabuleiro.pinos[i].ocupacao[h] = NUMERO_DISCOS - 1 - h;
            }
            else tabuleiro.pinos[i].ocupacao[h] = -1;
        }
    }

    //Pinos posicoes
    for (size_t i = 0; i < 3; i++)
    {
        for (size_t h = 0; h < NUMERO_DISCOS; h++)
        {
            double x = x_center + ((int)i - 1) * dx;
            double y = y_center;
            double z = (h + 1) * 0.3;
            CustomPoint& pos_to_set = tabuleiro.pinos[i].posicoes[h];
            pos_to_set.x = x;
            pos_to_set.y = y;
            pos_to_set.z = z;
        }
    }

    //2) Discos
    for (size_t i = 0; i < NUMERO_DISCOS; i++)
    {
        discos[i].posicao = tabuleiro.pinos[0].posicoes[NUMERO_DISCOS - i - 1];
    }
    //3) Disco ativo
    dico_ativo.index = -1;
    dico_ativo.em_movimento = false;
    dico_ativo.passo_u = 0.025;
    dico_ativo.u = 0.0;
    dico_ativo.direcao_rotacao = 0;
}

//Desenha um cilindro dada posicao, raio e altura
void DrawPino(double x, double y, double r, double h)
{
    GLUquadric* q = gluNewQuadric();
    GLint slices = 50;
    GLint stacks = 10;
    glPushMatrix();
    glTranslatef(x, y, 0.0f);
    gluCylinder(q, r, r, h, slices, stacks);
    glTranslatef(0, 0, h);
    gluDisk(q, 0, r, slices, stacks);
    glPopMatrix();
    gluDeleteQuadric(q);
}

//Desenha os pinos tendo o tabuleiro como base
void DrawTabuleiroPinos(Tabuleiro const &tabuleiro)
{
    //Materials,
    GLfloat cor_dourado[] = { 1.0f, 0.6f, 0.0f, 1.0f };
    GLfloat cor_branco[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glPushMatrix();
    //Drawing pinos and Pedestals
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cor_dourado);
    glRotatef(-90,1,0,0);
    double r = tabuleiro.raio_base_pinos;
    for (size_t i = 0; i < 3; i++)
    {
        CustomPoint const &p = tabuleiro.pinos[i].posicoes[0];
        if (selecionado == i)
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cor_branco);
        else
            glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, cor_dourado);

        DrawPino(p.x, p.y, r * 0.1, ALTURA_PINOS - 0.1);
        DrawPino(p.x, p.y, r, 0.1);
    }
    glPopMatrix();
}

// Desenha os discos
void DrawDiscos()
{
    int slices = 100;
    int stacks = 10;

    double raio;
    GLfloat r, g, b;
    GLfloat material[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    for (size_t i = 0; i < NUMERO_DISCOS; i++)
    {
        switch (i)
        {
            case 0: r = 1.0f, g = 0.0f; b = 1.0f;
                break;
            case 1: r = 0.0f, g = 1.0f; b = 1.0f;
                break;
            case 2: r = 0.0f; g = 1.0f; b = 0.0f;
                break;
            case 3: r = 1.0f, g = 1.0f; b = 0.0f;
                break;
            case 4: r = 0.0f, g = 0.0f; b = 1.0f;
                break;
            case 5: r = 1.0f; g = 0.0f; b = 0.0f;
                break;
            case 6: r = 0.5f, g = 0.5f; b = 1.0f;
                break;
            default: r = g = b = 1.0f;
                break;
        };

        material[0] = r;
        material[1] = g;
        material[2] = b;
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);

        GLfloat mult_size = 1.6f;
        switch (i) {
            case 0: mult_size = 0.2;
                break;
            case 1: mult_size = 0.4;
                break;
            case 2: mult_size = 0.6;
                break;
            case 3: mult_size = 0.8;
                break;
            case 4: mult_size = 1.0;
                break;
            case 5: mult_size = 1.2;
                break;
            case 6: mult_size = 1.4;
                break;
            default: break;
        };
        raio = mult_size * tabuleiro.raio_base_pinos;
        int direcao = dico_ativo.direcao_rotacao;

        glPushMatrix();
        glRotatef(-90,1,0,0);
        glTranslatef(discos[i].posicao.x, discos[i].posicao.y, discos[i].posicao.z);
        double rotacao = acos(discos[i].normal.z);
        rotacao *= 640.0f / M_PI;
        glRotatef(direcao * rotacao, 0.0f, 1.0f, 0.0f);
        glutSolidTorus(0.2 * tabuleiro.raio_base_pinos, raio, stacks, slices);
        glPopMatrix();
    }
}

void display_handler()
{
    /* limpar buffer */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    /* Posicionar a luz */
    lightPosition[0] = 12*cos(lightAngle);
    lightPosition[1] = lightHeight;
    lightPosition[2] = 12*sin(lightAngle);
    if (directionalLight) {
        lightPosition[3] = 0.0;
    } else {
        lightPosition[3] = 1.0;
    }

    shadowMatrix(floorShadow, floorPlane, lightPosition);

    glPushMatrix();
    /* Perform scene rotations based on user mouse input. */
    glRotatef(angle2, 1.0, 0.0, 0.0);
    glRotatef(angle, 0.0, 1.0, 0.0);

    /* Tell GL new light source posicao. */
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /* We can eliminate the visual "artifact" of seeing the "flipped"
    dinosaur underneath the floor by using stencil.  The idea is
    draw the floor without color or depth update but so that
    a stencil value of one is where the floor will be.  Later when
    rendering the dinosaur reflection, we will only update pixels
    with a stencil value of 1 to make sure the reflection only
    lives on the floor, not below the floor. */

    /* Don'para update color or depth. */
    glDisable(GL_DEPTH_TEST);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    /* Draw 1 into the stencil buffer. */
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
    glStencilFunc(GL_ALWAYS, 1, 0xffffffff);

    /* Now render floor; floor pixels just get their stencil set to 1. */
    drawFloor();

    /* Re-enable update of color and depth. */
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glEnable(GL_DEPTH_TEST);

    /* Now, only render where stencil is set to 1. */
    glStencilFunc(GL_EQUAL, 1, 0xffffffff);  /* draw if ==1 */
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);


    glPushMatrix();

    /* The critical reflection step: Reflect dinosaur through the floor
       (the Y=0 plane) to make a reflection. */
    glScalef(1.0, -1.0, 1.0);

    /* Reflect the light posicao. */
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /* To avoid our normals getting reversed and hence botched lighting
    on the reflection, turn on normalize.  */
    glEnable(GL_NORMALIZE);
    glCullFace(GL_FRONT);

    /* Desenha o reflexo */
    DrawTabuleiroPinos(tabuleiro);
    DrawDiscos();

    /* Disable noramlize again and re-enable back face culling. */
    glDisable(GL_NORMALIZE);
    glCullFace(GL_BACK);

    glPopMatrix();

    /* Switch back to the unreflected light posicao. */
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    glDisable(GL_STENCIL_TEST);

    /* Back face culling will get used to only draw either the top or the
       bottom floor.  This let's us get a floor with two distinct
       appearances.  The top floor surface is reflective and kind of red.
       The bottom floor surface is not reflective and blue. */

    /* Draw "bottom" of floor in blue. */
    glFrontFace(GL_CW);  /* Switch face orientation. */
    glColor4f(0.3, 0.3, 0.3, 1.0);
    drawFloor();
    glFrontFace(GL_CCW);

    /* Draw the floor with stencil value 3.  This helps us only
       draw the shadow once per floor pixel (and only on the
       floor pixels). */
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_ALWAYS, 3, 0xffffffff);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

    /* Draw "top" of floor.  Use blending to blend in reflection. */
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(1.0, 1.0, 1.0, 0.3);
    drawFloor();
    glDisable(GL_BLEND);

    /* Desenha o objeto real */
    DrawTabuleiroPinos(tabuleiro);
    DrawDiscos();

    /* Render the projected shadow. */

    /* Now, only render where stencil is set above 2 (ie, 3 where
    the top floor is).  Update stencil with 2 where the shadow
    gets drawn so we don'para redraw (and accidently reblend) the
    shadow). */
    glStencilFunc(GL_LESS, 2, 0xffffffff);  /* draw if ==1 */
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

    /* To eliminate depth buffer artifacts, we use polygon offset
    to raise the depth of the projected shadow slightly so
    that it does not depth buffer alias with the floor. */
    glEnable(GL_POLYGON_OFFSET_EXT);

    /* Render 50% black shadow color on top of whatever the
     floor appareance is. */
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_LIGHTING);  /* Force the 50% black. */
    glColor4f(0.0, 0.0, 0.0, 0.5);

    glPushMatrix();
    /* Project the shadow. */
    glMultMatrixf((GLfloat *) floorShadow);

    /* Desenha a sombra projetada */
    DrawTabuleiroPinos(tabuleiro);
    DrawDiscos();

    glPopMatrix();

    glDisable(GL_BLEND);
    glEnable(GL_LIGHTING);

    glDisable(GL_POLYGON_OFFSET_EXT);
    glDisable(GL_STENCIL_TEST);


    glPushMatrix();
    glDisable(GL_LIGHTING);
    glColor3f(1.0, 1.0, 0.0);
    if (directionalLight) {
        /* Desenha seta da luz */
        glDisable(GL_CULL_FACE);
        glTranslatef(lightPosition[0], lightPosition[1], lightPosition[2]);
        glRotatef(lightAngle * -180.0 / M_PI, 0, 1, 0);
        glRotatef(atan(lightHeight/12) * 180.0 / M_PI, 0, 0, 1);
        glBegin(GL_TRIANGLE_FAN);
        glVertex3f(0, 0, 0);
        glVertex3f(2, 1, 1);
        glVertex3f(2, -1, 1);
        glVertex3f(2, -1, -1);
        glVertex3f(2, 1, -1);
        glVertex3f(2, 1, 1);
        glEnd();
        /* Desenha a linha branca da luz */
        glColor3f(1.0, 1.0, 1.0);
        glBegin(GL_LINES);
        glVertex3f(0, 0, 0);
        glVertex3f(5, 0, 0);
        glEnd();
        glEnable(GL_CULL_FACE);
    } else {
        /* Desenha a bola amarela da luz */
        glTranslatef(lightPosition[0], lightPosition[1], lightPosition[2]);
        glutSolidSphere(1.0, 5, 5);
    }
    glEnable(GL_LIGHTING);
    glPopMatrix();

    glPopMatrix();

    glutSwapBuffers();
}

void reshape_handler(int w, int h)
{
    window_width = w;
    window_height = h;

    glViewport(0, 0, (GLsizei)w, (GLsizei)h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(
            /* field of view in degree */ FOV,
            /* aspect ratio */   (GLfloat)w/(GLfloat)h,
            /* Z near */                  1.0,
            /* Z far */                  100.0
    );

    glMatrixMode(GL_MODELVIEW);
}

void hanoi_rec(int n, int de, int para)
{
    if (n == 1) {
        par_de_solucao s;
        s.de = de;
        s.para = para;
        solucao.push_back(s); // guarda na lista para fazer animação depois
        cout << "Do pino " << de << " para o pino " << para << endl;
        return;
    }
    hanoi_rec(n - 1, de, 3 - para - de);
    hanoi_rec(1, de, para);
    hanoi_rec(n - 1, 3 - para - de, para);
}

void iniciar_resolucao()
{
    hanoi_rec(NUMERO_DISCOS, 0, 2); // num discos , pino inicial, pino final
}

void keyboard_handler(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27:
        case 'q':
        case 'Q':
            exit(0);
            break;
        case 'h':
        case 'H':
            print_info();
            break;
        case 'z':
        case 'Z':
            directionalLight = 0;
            break;
        case 'x':
        case 'X':
            directionalLight = 1;
            break;
        case 'c':
        case 'C':
            lightAutoMove = 1 - lightAutoMove;
            break;
        case 's':
        case 'S':
            if (tabuleiro.pinos[0].ocupacao[NUMERO_DISCOS - 1] < 0)
                break;
            iniciar_resolucao();
            resolver = true;
            break;
        case '+':
            if (FPS > 980) FPS = 1000;
            else FPS += 20;
            cout << "(+) Velocidade: " << FPS / 5.0 << "%" << endl;
            break;
        case '-':
            if (FPS <= 20) FPS = 2;
            else FPS -= 20;
            cout << "(-) Velocidade: " << FPS / 5.0 << "%" << endl;
            break;
        case 'j':
            if(selecionado < 3 && dico_ativo.em_movimento == false) {
                mover_disco(selecionado, 0);
                selecionado = 3;
            }
            else{
                selecionado = 3;
                for(int aux = 0; aux < NUMERO_DISCOS; aux++) {
                    if (tabuleiro.pinos[0].ocupacao[aux] != -1) {
                        selecionado = 0;
                        break;
                    }
                }
            }
            break;
        case 'k':
            if(selecionado < 3 && dico_ativo.em_movimento == false) {
                mover_disco(selecionado, 1);
                selecionado = 3;
            }
            else{
                selecionado = 3;
                for(int aux = 0; aux < NUMERO_DISCOS; aux++) {
                    if (tabuleiro.pinos[1].ocupacao[aux] != -1) {
                        selecionado = 1;
                        break;
                    }
                }
            }
            break;
        case 'l':
            if(selecionado < 3 && dico_ativo.em_movimento == false) {
                mover_disco(selecionado, 2);
                selecionado = 3;
            }
            else{
                selecionado = 3;
                for(int aux = 0; aux < NUMERO_DISCOS; aux++) {
                    if (tabuleiro.pinos[2].ocupacao[aux] != -1) {
                        selecionado = 2;
                        break;
                    }
                }
            }
            break;
        case 'f':
        case 'F':
            toggleFullScreen();
            break;
        default:
            break;
    };
    glutPostRedisplay();
}

void toggleFullScreen() {
    fullScreen = 1 - fullScreen;
    if (fullScreen) {
        glutFullScreen();
    } else {
        glutReshapeWindow(600, 600);
    }
}

void mover_disco(int de, int para)
{

    int d = para - de;
    if (d > 0) dico_ativo.direcao_rotacao = 1;
    else if (d < 0) dico_ativo.direcao_rotacao = -1;

    if ((de == para) || (de < 0) || (para < 0) || (de > 2) || (para > 2))
        return;

    size_t i;
    for (i = NUMERO_DISCOS - 1; i >= 0 && tabuleiro.pinos[de].ocupacao[i] < 0; i--);
    if ((i < 0) || (i == 0 && tabuleiro.pinos[de].ocupacao[i] < 0))
        return; //Ou o índice < 0 ou índice em 0 tem ocupação < 0 (é um Pinos vazio)

    dico_ativo.pos_inicio = tabuleiro.pinos[de].posicoes[i];

    dico_ativo.index = tabuleiro.pinos[de].ocupacao[i];
    dico_ativo.em_movimento = true;
    dico_ativo.u = 0.0;


    size_t j;
    for (j = 0; j < NUMERO_DISCOS - 1 && tabuleiro.pinos[para].ocupacao[j] >= 0; j++);
    dico_ativo.pos_fim = tabuleiro.pinos[para].posicoes[j];

    tabuleiro.pinos[de].ocupacao[i] = -1;
    tabuleiro.pinos[para].ocupacao[j] = dico_ativo.index;
}

// calcula o proximo passo da animação do movimento do disco
CustomPoint get_inerpolated_coordinate(CustomPoint sp, CustomPoint tp, double u)
{
    //4 Control points
    CustomPoint p;
    double x_center = 0;
    double y_center = 0;

    double u3 = u * u * u;
    double u2 = u * u;

    CustomPoint cps[4]; //P1, P2, dP1, dP2

    {//interpolação de Hermite
        //P1
        cps[0].x = sp.x;
        cps[0].y = y_center;
        cps[0].z = ALTURA_PINOS + 0.2 * (tabuleiro.raio_base_pinos);

        //P2
        cps[1].x = tp.x;
        cps[1].y = y_center;
        cps[1].z = ALTURA_PINOS + 0.2 * (tabuleiro.raio_base_pinos);

        //dP1
        cps[2].x = (sp.x + tp.x) / 2.0 - sp.x;
        cps[2].y = y_center;
        cps[2].z = 2 * cps[1].z; //change 2 * ..

        //dP2
        cps[3].x = tp.x - (tp.x + sp.x) / 2.0;
        cps[3].y = y_center;
        cps[3].z = -cps[2].z; //- cps[2].z;

        double h0 = 2 * u3 - 3 * u2 + 1;
        double h1 = -2 * u3 + 3 * u2;
        double h2 = u3 - 2 * u2 + u;
        double h3 = u3 - u2;

        p.x = h0 * cps[0].x + h1 * cps[1].x + h2 * cps[2].x + h3 * cps[3].x;
        p.y = h0 * cps[0].y + h1 * cps[1].y + h2 * cps[2].y + h3 * cps[3].y;
        p.z = h0 * cps[0].z + h1 * cps[1].z + h2 * cps[2].z + h3 * cps[3].z;
    }
    return p;
}

void normalize(CustomPoint& v)
{
    double length = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    if (length == 0.0) return;
    v.x /= length;
    v.y /= length;
    v.z /= length;
}


void anim_handler()
{
    int curr_time = glutGet(GLUT_ELAPSED_TIME);
    int elapsed = curr_time - prev_time; // em ms
    if (elapsed < 1000 / FPS) return;

    prev_time = curr_time;

    if (!lightManualMoving && lightAutoMove) {
        lightAngle += 0.03;
    }

    if (resolver && dico_ativo.em_movimento == false) {
        par_de_solucao s = solucao.front();

        cout << "Do pino " << s.de << " para o pino " << s.para << endl;

        solucao.pop_front();
        int i;
        for (i = NUMERO_DISCOS; i >= 0 && tabuleiro.pinos[s.de].ocupacao[i] < 0; i--);
        int ind = tabuleiro.pinos[s.de].ocupacao[i];

        if (ind >= 0)
            dico_ativo.index = ind;

        mover_disco(s.de, s.para);
        if (solucao.size() == 0)
            resolver = false;
    }

    if (dico_ativo.em_movimento)
    {
        int ind = dico_ativo.index;
        DiscoAtivo& discoAtivo = dico_ativo;

        if (discoAtivo.u == 0.0 && (discos[ind].posicao.z < ALTURA_PINOS + 0.2 * (tabuleiro.raio_base_pinos)))
        {
            discos[ind].posicao.z += 0.05;
            glutPostRedisplay();
            return;
        }

        static bool done = false;
        if (discoAtivo.u == 1.0 && discos[ind].posicao.z > discoAtivo.pos_fim.z)
        {
            done = true;
            discos[ind].normal = CustomPoint(0, 0, 1);
            discos[ind].posicao.z -= 0.05;
            glutPostRedisplay();
            return;
        }

        discoAtivo.u += discoAtivo.passo_u;
        if (discoAtivo.u > 1.0) {
            discoAtivo.u = 1.0;
        }

        if (!done) {
            CustomPoint prev_p = discos[ind].posicao;
            CustomPoint p = get_inerpolated_coordinate(discoAtivo.pos_inicio, discoAtivo.pos_fim, discoAtivo.u);
            discos[ind].posicao = p;
            discos[ind].normal.x = (p - prev_p).x;
            discos[ind].normal.y = (p - prev_p).y;
            discos[ind].normal.z = (p - prev_p).z;
            normalize(discos[ind].normal);
        }

        if (discoAtivo.u >= 1.0 && discos[ind].posicao.z <= discoAtivo.pos_fim.z) {
            discos[ind].posicao.z = discoAtivo.pos_fim.z;
            discoAtivo.em_movimento = false;
            done = false;
            discoAtivo.u = 0.0;
            discos[discoAtivo.index].normal = CustomPoint(0, 0, 1);
            discoAtivo.index = -1;
        }
        glutPostRedisplay();
    } else if (lightAutoMove) {// atualiza para mover apenas a luz.
        glutPostRedisplay();
    }
}

/* Para animação quando não visivel. */
void visible(int vis)
{
    if (vis == GLUT_VISIBLE) {
        if (animation)
            glutIdleFunc(anim_handler);
    } else {
        if (!animation)
            glutIdleFunc(NULL);
    }
}

// Menu handling function definition
void menu(int item)
{
    switch (item)
    {
        case M_NONE:
            return;
        case M_PAUSE:
            animation = 1 - animation;
            if (animation) {
                glutIdleFunc(anim_handler);
            } else {
                glutIdleFunc(NULL);
            }
            break;
        case LIGHT_AUTO_MOTION:
            lightAutoMove = 1 - lightAutoMove;
            break;
        case M_POSITIONAL:
            directionalLight = 0;
            break;
        case M_DIRECTIONAL:
            directionalLight = 1;
            break;
        case MENU_AJUDA:
            print_info();
            break;
        case MENU_RESOLVER:
            if (tabuleiro.pinos[0].ocupacao[NUMERO_DISCOS - 1] < 0)
                break;
            iniciar_resolucao();
            resolver = true;
            break;
        case MENU_INC_VEL:
            if (FPS > 980) FPS = 1000;
            else FPS += 20;
            cout << "(+) Velocidade: " << FPS / 5.0 << "%" << endl;
            break;
        case MENU_DEC_VEL:
            if (FPS <= 20) FPS = 2;
            else FPS -= 20;
            cout << "(-) Velocidade: " << FPS / 5.0 << "%" << endl;
            break;
        case MENU_FULL_SCREEN:
            toggleFullScreen();
            break;
        case MENU_Exit:
            exit(0);
            break;
        default:
            break;
    }
    glutPostRedisplay();
    return;
}